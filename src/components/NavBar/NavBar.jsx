import React from "react";
import "./NavBar.css"
import {Link} from "react-router-dom";

const NavBar = (props) => {
    return(
        <div className="header">
            <div><img src={require("../../imgs/xoxo_logo.svg")} alt="logo"/></div>
            <div className="NavBar">
                <Link to={"/game-field"}>Игровое поле</Link>
                <Link to={"/raiting"}>Рейтинг</Link>
                <Link to={"/active-players"}>Активные игроки</Link>
                <Link to={"/game-history"}>История игр</Link>
                <Link to={"/player-list"}>Список игроков</Link>
            </div>
            <Link to={"/"}>
            <img alt="Quit" src={require("../../imgs/quit_button.svg")} 
            onMouseOver={e=> e.currentTarget.src = require("../../imgs/quit_button_hover.svg")}
            onMouseOut={e=> e.currentTarget.src = require("../../imgs/quit_button.svg")}/>
            </Link>
      </div>
    );
};

export default NavBar;