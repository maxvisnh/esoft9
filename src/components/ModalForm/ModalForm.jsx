import React from "react";
import "./Modal.css"



const ModalForm = ({active, setActive, children}) => {
const clickfunc = () =>{
    setActive(false); 
    console.log(active)
}

    return(
        <div className={active === true ? "modalForm active" : "modalForm"} onClick={clickfunc}>
            <div className={active === true ? "modalContent active" : "modalContent"} onClick={(e)=>e.stopPropagation()}>
                {children}
            </div>
        </div>
    )
}

export default ModalForm;