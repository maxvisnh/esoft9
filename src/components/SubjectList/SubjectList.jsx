import React from "react";
import "./SubjectList.css";

function SubjectList(){
    return(
        <div className="subject-list">
            <h1>Игроки</h1>
            <div className="container">
            <div className="subject-container">
                <div><img className="subject-icon" alt="zero" src={require("../../imgs/zero.svg")}/></div>
                <div className="subject-info">
                <div className="subject-name">Пупкин Владелен Игоревич</div>
                <div className="subject-percent">63% побед</div>
                </div>
            </div>
            <div className="subject-container">
                <div><img className="subject-icon" alt="x" src={require("../../imgs/x.svg")} /></div>
                <div className="subject-info">
                <div className="subject-name">Плюшкина Екатерина Викторовна</div>
                <div className="subject-percent">23% побед</div>
                </div>
            </div>
            </div>
        </div>
    );
}

export default SubjectList;