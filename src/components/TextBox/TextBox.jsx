import React from "react";
import classes from "./TextBox.module.css"

const TextBox = (props) => {
    return(
        <input 
        {...props} 
        className={classes.TextBox} 
        />
    );
};

export default TextBox;