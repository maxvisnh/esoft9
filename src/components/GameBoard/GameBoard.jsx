import React, {useState} from "react";
import "./GameBoard.css"

function Gameboard (){
    const turnSwitch = document.getElementsByClassName("turn-img");


    const [turn, setTurn] = useState("x");
    /* let game = [
        ['', '', ''],
        ['', '', ''],
        ['', '', '']
    ]; */
    const cellClick =(e)=>{
        alert(e.target.id);
        if(turn === "x"){
            e.target.src = require("../../imgs/xxl-x.svg");
            turnSwitch.src = "../../imgs/zero.svg";
            setTurn("o");
        }
        else{
            e.target.src = require("../../imgs/xxl-zero.svg");
            setTurn("x");
        }
    };
    
    return(
        <div className="game-container">
            <div className="game-time">05:12</div>
                <div className="game-board">
                    <button className="cell" id="1"><img  id="cell1Img" onClick={cellClick} style={{width:"100%", height:"100%"}}/></button >
                    <button className="cell" id="2"><img  id="cell2Img" onClick={cellClick} style={{width:"100%", height:"100%"}}/></button >
                    <button className="cell" id="3"><img  id="cell3Img" onClick={cellClick} style={{width:"100%", height:"100%"}}/></button >
                    <button className="cell" id="4"><img  id="cell4Img" onClick={cellClick} style={{width:"100%", height:"100%"}}/></button >
                    <button className="cell" id="5"><img  id="cell5Img" onClick={cellClick} style={{width:"100%", height:"100%"}}/></button >
                    <button className="cell" id="6"><img  id="cell6Img" onClick={cellClick} style={{width:"100%", height:"100%"}}/></button >
                    <button className="cell" id="7"><img  id="cell7Img" onClick={cellClick} style={{width:"100%", height:"100%"}}/></button >
                    <button className="cell" id="8"><img  id="cell8Img" onClick={cellClick} style={{width:"100%", height:"100%"}}/></button >
                    <button className="cell" id="9"><img  id="cell9Img" onClick={cellClick} style={{width:"100%", height:"100%"}}/></button >
                </div>
                <div className="game-step" >Ходит&nbsp;
                    <img 
                        className="turn-img" 
                        id="turn" 
                        alt="turn" 
                        src={require("../../imgs/x.svg")}
                        />
                    &nbsp;игрок
                </div>
        </div>
    );
}

export default Gameboard;
