import React from "react";
import classes from "./GreenBtn.module.css"

const GreenBtn = ({children,...props}) => {
    return(
        <button {...props} className={classes.GreenBtn}>
            {children}
        </button>
    );
};

export default GreenBtn;