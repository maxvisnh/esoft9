import React from "react";
import classes from "./StratusBlock.module.css"

function StausBlock({children, ...props}) {
    return (
        <div {...props} className={children === "Активен" || children === "Свободен" ?  classes.StatusBlockGreen : classes.StatusBlockRed}>
           {children}
        </div>
    );
}
  export default StausBlock;