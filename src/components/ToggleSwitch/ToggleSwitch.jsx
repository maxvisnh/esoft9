import React, { useState } from 'react';
import styled from 'styled-components';

export default ({ checked = false, color ='#F7F7F7' , ...props}) => {
  const [toggle, setToggle] = useState(checked)

  return (
    <Switch {...props}>
      <Input type='checkbox' />
      <Slider {...{ toggle, color }} onClick={() => setToggle(!toggle)} />
    </Switch>
  )
}

const Slider = styled.span`
  position: absolute;
  margin-left: 10px;
  padding-top: 25px;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0; 
  bottom: 0;
  background-color: ${({ toggle, color }) => toggle ? "#e0f3e8" : '#DCDCDF'};
  border-radius: 15px;
  transition: 0.4s;
  &:before {
    content: '';
    position: absolute;
    left: 2px;
    bottom: 2px;
    width: 20px;
    height: 20px;
    border-radius: 100%;
    background-color: ${({ toggle, color }) => toggle ? '#60C2AA' : color};
    transition: 0.4s;
  }
`

const Input = styled.input`
  &:checked + ${Slider} {
    background-color: ${({ color }) => color};
  }
  &:checked + ${Slider}:before {
    transform: translateX(15px);
  } 
`

const Switch = styled.label`
  position: absolute;
  display: inline-block;
  width: 50px;
  height: 15px;
  & ${Input} {
    opacity: 0;
    width: 0;
    height: px;
  }
`