import React from "react";
import { BrowserRouter as Router, Route, Routes} from "react-router-dom";
import Auth from "./pages/Auth";
import NavBar from "./components/NavBar/NavBar";
import Raiting from "./pages/Raiting";
import ActivePlayers from "./pages/ActivePlayers";
import GameHistory from "./pages/GameHistory";
import PlayerList from "./pages/PlayerList";
import GameField from "./pages/GameField";

function App() {
  return (
    <Router>
      <NavBar></NavBar>
      <Routes>
        <Route path="/" element={<Auth/>}/>
        <Route path="/raiting" element={<Raiting/>}/>
        <Route path="/active-players" element={<ActivePlayers/>}/>
        <Route path="/game-history" element={<GameHistory/>}/>
        <Route path="/player-list" element={<PlayerList/>}/>
        <Route path="/game-field" element={<GameField/>}/>
      </Routes>
    </Router>
  );
}

export default App;
