import React from "react";
import "../css/Raiting.css";

let players =[
    { id:1, name: 'Александров Игнат Анатолиевич', gameCount: "431124", win: 2012, lose:423, winRate:"24%", status:true, block:false},
    { id:2, name: 'Александров Игнат Анатолиевич', gameCount: "431124", win: 2012, lose:423, winRate:"24%", status:true, block:false},
    { id:3, name: 'Александров Игнат Анатолиевич', gameCount: "431124", win: 2012, lose:423, winRate:"24%", status:true, block:false},
    { id:4, name: 'Александров Игнат Анатолиевич', gameCount: "431124", win: 2012, lose:423, winRate:"24%", status:true, block:false},
    { id:5, name: 'Александров Игнат Анатолиевич', gameCount: "431124", win: 2012, lose:423, winRate:"24%", status:true, block:false},
    { id:6, name: 'Александров Игнат Анатолиевич', gameCount: "431124", win: 2012, lose:423, winRate:"24%", status:true, block:false},
    { id:7, name: 'Александров Игнат Анатолиевич', gameCount: "431124", win: 2012, lose:423, winRate:"24%", status:true, block:false},
    { id:8, name: 'Александров Игнат Анатолиевич', gameCount: "431124", win: 2012, lose:423, winRate:"24%", status:true, block:false},
    { id:9, name: 'Александров Игнат Анатолиевич', gameCount: "431124", win: 2012, lose:423, winRate:"24%", status:true, block:false},
    { id:10, name: 'Александров Игнат Анатолиевич', gameCount: "431124", win: 2012, lose:423, winRate:"24%", status:true, block:false},
    { id:11, name: 'Александров Игнат Анатолиевич', gameCount: "431124", win: 2012, lose:423, winRate:"24%", status:true, block:false},
    { id:12, name: 'Александров Игнат Анатолиевич', gameCount: "431124", win: 2012, lose:423, winRate:"24%", status:true, block:false},
    { id:13, name: 'Александров Игнат Анатолиевич', gameCount: "431124", win: 2012, lose:423, winRate:"24%", status:true, block:false},
    { id:14, name: 'Александров Игнат Анатолиевич', gameCount: "431124", win: 2012, lose:423, winRate:"24%", status:true, block:false}
]

function Raiting() {
  return (
    <div className="main-container-raiting">
        <h1>Рейтинг игроков</h1>
        <table className="main-table">
            <thead>
                <tr>
                    <th>ФИО</th>
                    <th>Всего игр</th>
                    <th>Победы</th>
                    <th>Проигрыши</th>
                    <th>Процент побед</th>
                </tr>
            </thead>
            <tbody>
                {players.map(item=>(
                <tr key={item.id}>
                    <td >{item.name}</td>
                    <td >{item.gameCount}</td>
                    <td  className="win">{item.win}</td>
                    <td  className="lose">{item.lose}</td>
                    <td >{item.winRate}</td>
                </tr>
                ))}
            </tbody>
      </table>
    </div>
  );
}

export default Raiting;
