import React from "react";
import "../css/GameField.css";
import SubjectList from "../components/SubjectList/SubjectList";
import Gameboard from "../components/GameBoard/GameBoard";
import Chat from "../components/Chat/Chat";

function GameField(){
    return(
        <div className="game-field">
            <SubjectList></SubjectList>
            <Gameboard></Gameboard>
            <Chat></Chat>
        </div>
    );
}

export default GameField;