import React, { useState } from "react";
import TextBox from "../components/TextBox/TextBox";
import "../css/Auth.css"
import GreenBtn from "../components/GreenBtn/GreenBtn";

function Auth() {
  const [loginContent, setLoginContent] = useState("");
  const [passwordContent, setPasswordContent] = useState("");

  return (
    <div className="Auth">
      <div className="login-block">
        <img src={require("../imgs/welcome_dog.svg")} alt="Войдите в игру" className="login-image"></img>
        <h1>Войдите в игру</h1>
        <TextBox 
        value = {loginContent}
        onChange = {e => setLoginContent(e.target.value)}
        type ="text" 
        placeholder = "Логин" 
        />
        <TextBox 
        value = {passwordContent}
        
        onChange = {e => setPasswordContent(e.target.value)}
        type ="text" 
        placeholder = "Пароль" 
        />
        {
          loginContent !== "" && passwordContent !== ""
          ?
          <GreenBtn >Войти</GreenBtn>
          :
          <GreenBtn disabled>Войти</GreenBtn>
        }
      </div>
    </div>
  );
}

export default Auth;