import React, { useState } from "react";
import GreenBtn from "../components/GreenBtn/GreenBtn";
import "../css/PlayerList.css";
import StatusBlock from "../components/StatusBlock/StatusBlock";
import ModalForm from "../components/ModalForm/ModalForm";
import TextBox from "../components/TextBox/TextBox";

let players =[
    { id:1, name: 'Александров Игнат Анатолиевич', age: 12, sex: "man", status:"unblock", create:"12 октября 2021%", changed:"22 октября 2021"},
    { id:2, name: 'Александров Игнат Анатолиевич', age: 44, sex: "woman", status:"block", create:"12 октября 2021%", changed:"22 октября 2021"},
    { id:3, name: 'Александров Игнат Анатолиевич', age: 21, sex: "woman", status:"unblock", create:"12 октября 2021%", changed:"22 октября 2021"},
    { id:4, name: 'Александров Игнат Анатолиевич', age: 53, sex: "woman", status:"unblock", create:"12 октября 2021%", changed:"22 октября 2021"},
    { id:5, name: 'Александров Игнат Анатолиевич', age: 11, sex:"man", status:"unblock", create:"12 октября 2021%", changed:"22 октября 2021"},
    { id:6, name: 'Александров Игнат Анатолиевич', age: 22, sex: "woman", status:"unblock", create:"12 октября 2021%", changed:"22 октября 2021"},
    { id:7, name: 'Александров Игнат Анатолиевич', age: 32, sex: "woman", status:"block", create:"12 октября 2021%", changed:"22 октября 2021"},
    { id:8, name: 'Александров Игнат Анатолиевич', age: 22, sex: "man", status:"unblock", create:"12 октября 2021%", changed:"22 октября 2021"},
    { id:9, name: 'Александров Игнат Анатолиевич', age: 52, sex: "woman", status:"block", create:"12 октября 2021%", changed:"22 октября 2021"},
    { id:10, name: 'Александров Игнат Анатолиевич', age: 14, sex: "woman", status:"block", create:"12 октября 2021%", changed:"22 октября 2021"},
    { id:11, name: 'Александров Игнат Анатолиевич', age: 14, sex: "man", status:"unblock", create:"12 октября 2021%", changed:"22 октября 2021"},
    { id:12, name: 'Александров Игнат Анатолиевич', age: 5, sex: "man", status:"block", create:"12 октября 2021%", changed:"22 октября 2021"},
    { id:13, name: 'Александров Игнат Анатолиевич', age: 4, sex: "man", status:"unblock", create:"12 октября 2021%", changed:"22 октября 2021"},
    { id:14, name: 'Александров Игнат Анатолиевич', age: 12, sex: "man", status:"unblock", create:"12 октября 2021%", changed:"22 октября 2021"}
]

function PlayerList(){
    const [modalActive, setModalActive] = useState(false);
    return(
        <div className="main-list">
            
            <ModalForm active={modalActive} setActive={setModalActive}>
                <div>
                    <img alt="exit" src={require("../imgs/exit.svg")} onClick={()=>setModalActive(false)} style={{cursor:"pointer"}}></img>
                </div>
                <div>
                    <h1>Добавте игрока</h1>
                </div>
                <div>
                    <label style={{marginLeft:"12px"}}>ФИО</label>
                    <TextBox placeholder="Иванов Иван Иванович" style={{width:"94%"}}></TextBox>
                </div>
                <div>
                    <label style={{marginLeft:"12px"}}>Возраст</label>
                    <TextBox placeholder="0"></TextBox>
                </div> 
                <div>
                    <label>Пол</label>
                    <label class="custom-radio-woman">
                        <input type="radio" name="sex" value="woman"/>
                        <span></span>
                    </label>
                    <label class="custom-radio">
                        <input type="radio" name="sex" value="man"/>
                        <span></span>
                    </label>
                </div>
                <div>
                    <GreenBtn style={{marginLeft:"12px", width:"94%"}}>Добавить</GreenBtn>
                </div>   
            </ModalForm>
            <div className="first-string-pl">
                <h1>Список игроков</h1>
                <GreenBtn onClick={()=>setModalActive(true)} style ={{width:"12%"}}>Добавить игрока</GreenBtn>
            </div>
            <table className="table-list">
                <thead>
                    <tr>
                        <th>ФИО</th>
                        <th>Возраст</th>
                        <th>Пол</th>
                        <th>Статус</th>
                        <th>Создан</th>
                        <th>Изменен</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {players.map(item=>(
                    <tr key={item.id}>
                        <td >{item.name}</td>
                        <td >{item.age}</td>
                        {item.sex==="man"
                        ?
                        <td><img alt="man" src={require("../imgs/man.svg")}></img></td>
                        :
                        <td><img alt="woman" src={require("../imgs/woman.svg")}></img></td>
                        }
                        {item.status==="unblock"
                        ?
                        <td><StatusBlock style={{width:"140px", height:"40px"}}>Активен</StatusBlock></td>
                        :
                        <td><StatusBlock style={{width:"140px", height:"40px"}}>Заблокирован</StatusBlock></td>
                        }
                        <td >{item.create}</td>
                        <td >{item.changed}</td>
                        {item.status==="unblock"
                        ?
                        <td><GreenBtn style={{marginTop:"0px", backgroundColor:"#F7F7F7", color:"#373745"}}><img alt="block" src={require("../imgs/block.svg")}/>Заблокировать</GreenBtn></td>
                        :
                        <td><GreenBtn style={{marginTop:"0px", backgroundColor:"#F7F7F7", color:"#373745"}}>Разблокировать</GreenBtn></td>
                        }
                    </tr>
                    ))}
                </tbody>
            </table>
            
        </div>
    );
}

export default PlayerList;