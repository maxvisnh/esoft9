import React from "react";
import "../css/ActivePlayers.css";
import GreenBtn from "../components/GreenBtn/GreenBtn";
import ToggleSwitch from "../components/ToggleSwitch/ToggleSwitch";
import StausBlock from "../components/StatusBlock/StatusBlock";


let players =[
    { id:111, name: 'Александров Игнат Анатолиевич', status: "В игре"},
    { id:222, name: 'Александров Игнат Анатолиевич', status: "Свободен"},
    { id:333, name: 'Александров Игнат Анатолиевич', status: "Свободен"},
    { id:444, name: 'Александров Игнат Анатолиевич', status: "Свободен"},
    { id:555, name: 'Александров Игнат Анатолиевич', status: "В игре"},
    { id:666, name: 'Александров Игнат Анатолиевич', status: "Свободен"},
    { id:777, name: 'Александров Игнат Анатолиевич', status: "Свободен"},
    { id:888, name: 'Александров Игнат Анатолиевич', status: "Свободен"},
    { id:999, name: 'Александров Игнат Анатолиевич', status: "В игре"},
    { id:100, name: 'Александров Игнат Анатолиевич', status: "Свободен"},
    { id:110, name: 'Александров Игнат Анатолиевич', status: "Свободен"},
    { id:120, name: 'Александров Игнат Анатолиевич', status: "Свободен"}
]
//const filtredPlayers =players.map(item => item.status === "Свободен");

function ActivePlayers() {

  return (
    <div className="main-container-active-players">
        <div className="first-string">
            <h1>Активные игроки</h1>
            <h3>Только свободные 
                <ToggleSwitch/>
            </h3>
        </div>
        <table className="main-table-active-players">
            <tbody>
                {players.map(item=>(
                <tr key={item.id}>
                    <td >{item.name}</td>
                    <td ><StausBlock>{item.status}</StausBlock></td>
                    {item.status === "В игре" 
                    ? 
                    <GreenBtn disabled>Позвать играть</GreenBtn>
                    :
                    <GreenBtn>Позвать играть</GreenBtn>
                    }
                </tr>
                ))}
            </tbody>
      </table>
    </div>
  );
}

export default ActivePlayers;
