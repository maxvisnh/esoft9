import React from "react";
import "../css/GameHistory.css";


let games =[
    { id:11, playerOne: 'Александров Игнат ', playerTwo: "Моника Гастомбита", rusult: 1, date:"13 января 2023", time:"2:15"},
    { id:22, playerOne: 'Александров Игнат ', playerTwo: "Моника Гастомбита", rusult: 2, date:"13 января 2023", time:"2:15"},
    { id:33, playerOne: 'Александров Игнат ', playerTwo: "Моника Гастомбита", rusult: 1, date:"13 января 2023", time:"2:15"},
    { id:44, playerOne: 'Александров Игнат ', playerTwo: "Моника Гастомбита", rusult: 2, date:"13 января 2023", time:"2:15"},
    { id:55, playerOne: 'Александров Игнат ', playerTwo: "Моника Гастомбита", rusult: 2, date:"13 января 2023", time:"2:15"},
    { id:66, playerOne: 'Александров Игнат ', playerTwo: "Моника Гастомбита", rusult: 2, date:"13 января 2023", time:"2:15"},
    { id:77, playerOne: 'Александров Игнат ', playerTwo: "Моника Гастомбита", rusult: 1, date:"13 января 2023", time:"2:15"},
    { id:88, playerOne: 'Александров Игнат ', playerTwo: "Моника Гастомбита", rusult: 1, date:"13 января 2023", time:"2:15"},
    { id:99, playerOne: 'Александров Игнат ', playerTwo: "Моника Гастомбита", rusult: 2, date:"13 января 2023", time:"2:15"},
    { id:100, playerOne: 'Александров Игнат ', playerTwo: "Моника Гастомбита", rusult: 2, date:"13 января 2023", time:"2:15"},
    { id:10, playerOne: 'Александров Игнат ', playerTwo: "Моника Гастомбита", rusult: 2, date:"13 января 2023", time:"2:15"},
    { id:20, playerOne: 'Александров Игнат ', playerTwo: "Моника Гастомбита", rusult: 1, date:"13 января 2023", time:"2:15"}
]

function GameHistory() {
  return (
    <div className="main-history">
      <h1>История игр</h1>
        <table className="table-history">
            <thead>
                <tr>
                    <th>Игроки</th>
                    <th></th>
                    <th></th>
                    <th>Дата</th>
                    <th>Время игры</th>
                </tr>
            </thead>
            <tbody>
                {games.map(item=>(
                <tr key={item.id}>
                    <td style={{width:"25%"}}  >
                        <img className="img-sym" alt="zero" src={require("../imgs/zero_small.svg")}/>   
                        {item.playerOne} 
                        {item.rusult===1 ? <img className="img-win" alt="win" src={require("../imgs/win.svg")}/>: null}
                    </td>
                    <td ><strong>против</strong> </td>
                    <td > 
                        <img className="img-sym" alt="x" src={require("../imgs/x_small.svg")}/>
                        {item.playerTwo}
                        {item.rusult===2 ? <img className="img-win" alt="win" src={require("../imgs/win.svg")}/>: null}
                    </td>
                    <td style={{width:"14%"}} >{item.date}</td>
                    <td style={{width:"11%"}} >{item.time}</td>
                </tr>
                ))}
            </tbody>
      </table>
    </div>
  );
}

export default GameHistory;
